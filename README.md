# Functional programming helpers
When I come across something I want to write in a more pure functional way, I'll try and add it here. Inspired by [RamdJS](http://ramdajs.com).

This is an experiment for me and is not necessarily the best solution to your problem. Still exploring with go's typing system. Might throw this out once I get a better grip of it!

For the avoidance of doubt, the name came from mashing "func" and "funky" together. 

