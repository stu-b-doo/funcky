package funcky

import (
	"fmt"
	"os"
)

// First func(a []interface{}) returns the first element of a if len(a) > 1, nil otherwise.
var First = CNth(0)

func StrFirst(a []string) string {

	// assert type string
	if s, ok := First(a).(string); ok {
		return s
	} else {
		return ""
	}
}

// Nth returns a[n] or, if a[n] is out of bounds, nil.
func Nth(n int, a ...interface{}) interface{} {
	// implemented by CNth
	return CNth(n)(a)
}

func StrNth(n int, a []string) string {
	if s, ok := Nth(n, a).(string); ok {
		return s
	} else {
		return ""
	}
}

// CNth is a curried version of Nth.
func CNth(n int) func(a ...interface{}) interface{} {

	return func(a ...interface{}) interface{} {

		if len(a) > n {
			return a[n]
		} else {
			return nil
		}
	}
}

// Must wraps a func returning a value, passing the error and a message through FatalIf.
// Passes a message to FatalIf by Currying, since I don't know if there's another way to expand the args returned by the function and append msg
// TODO would it be better if this panicked instead? Inspired by https://golang.org/pkg/html/template/#Must
func Must(a interface{}, e error) func(msg ...interface{}) interface{} {

	return func(msg ...interface{}) interface{} {
		FatalIf(e, msg)
		return a
	}
}

func StrMust(s string, e error) func(msg ...interface{}) string {
	// Must always returns its first arg so the type may be asserted
	return func(msg ...interface{}) string {
		return Must(s, e)(msg).(string)
	}
}

// FatalIf prints an error and terminates program if error is not nil
func FatalIf(e error, msg ...interface{}) {
	if e != nil {
		fmt.Println(e, msg)
		os.Exit(1)
	}
}
